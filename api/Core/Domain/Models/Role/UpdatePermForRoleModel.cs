﻿namespace Core.Models
{
	public class UpdatePermForRoleModel
	{
		public long RoleId {  get; set; }
		public List<long> PermIds { get; set; }
	}
}
