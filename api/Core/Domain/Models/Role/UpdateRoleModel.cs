﻿namespace Core.Models
{
	public class UpdateRoleModel
	{
		public long Id { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
	}
}
