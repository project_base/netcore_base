﻿using Domain.Schemas;
using Microsoft.EntityFrameworkCore;

namespace Core
{
    public partial class DataContext : DbContext
    {
        public DataContext() {}

        public DataContext(DbContextOptions<DataContext> options)
            : base(options) {}

        public virtual DbSet<UserSchema> Users { get; set; }
        public virtual DbSet<RoleSchema> Roles { get; set; }
        public virtual DbSet<PermSchema> Perms { get; set; }
        public virtual DbSet<RolesPerms> RolesPerms { get; set; }
        public virtual DbSet<UsersRoles> UsersRoles { get; set; }
	}
}
