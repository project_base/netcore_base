﻿namespace Core.Domain.Business
{
	public class GroupRule
	{
		public enum RoleUser
		{
			ADMIN = 1,
			CENSOR = 2,
			MEMBER = 3,
		}

		public enum GroupStatus
		{
			INACTIVE = 1,
			ACTIVE = 2,
		}

		public enum MemberStatus
		{
			PENDING = 1,
			ACCEPTED = 2
		}
	}
}
