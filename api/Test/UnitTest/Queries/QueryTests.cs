﻿using Application.Queries;
using System.Reflection;

namespace Application.UnitTest.Queries
{
    public class QueryTests
    {
        [Fact]
        public void AllQueriesShouldHaveParameterlessConstructor()
        {
            var queryInterfaceType = typeof(IQuery<,>);
            var assembly = Assembly.GetAssembly(typeof(IQuery<,>)); // Get the assembly containing IQuery implementations
            var queryImplementations = assembly!.GetTypes()
                .Where(t => t.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == queryInterfaceType));
            foreach (var queryImplementation in queryImplementations)
            {
                var constructors = queryImplementation.GetConstructors();
                Assert.True(constructors.Any(c => c.GetParameters().Length == 0),
                    $"{queryImplementation.Name} does not have a parameterless constructor.");

            }

        }
    }
}
