﻿using Application.Queries;
using Application.Services;
using Domain;
using Moq;
using Serilog.Events;
using Serilog;
using System.Reflection;
using Application.UnitTest.TestHelpers.Serilog;
using Application.Exceptions;

namespace Application.UnitTest.Queries.Executor
{
    public class QueryExecutorTest
    {
        [Fact]
        public async Task QueryAsync_Returns_Default_Value_If_Query_Is_Not_Enable()
        {
            var userProvider = new Mock<IUserProvider>();
            userProvider.Setup(u => u.User).Returns(new FakeUser());
            var featureToggleService = new Mock<IFeatureToggleService>();
            featureToggleService.Setup(f => f.IsOnForType(It.IsAny<Type>())).Returns(false);
            var serviceProvider = new Mock<IServiceProvider>();
            var executor = new Mock<IQueryExecutor<TestQuery, string>>();
            executor.Setup(e => e.AuthoriseAsync(It.IsAny<TestQuery>(), It.IsAny<IUser>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(false);
            executor.Setup(e => e.ExecuteAsync(It.IsAny<TestQuery>(), It.IsAny<IUser>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync("Result");
            serviceProvider.Setup(s => s.GetService(typeof(IQueryExecutor<TestQuery, string>))).Returns(executor.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IUserProvider))).Returns(userProvider.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IFeatureToggleService))).Returns(featureToggleService.Object);
            var query = new TestQuery();
            var queryExecutor = new QueryExecutor(serviceProvider.Object);
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            // Replace the logger instance with the mock logger
            typeof(QueryExecutor)
                .GetField("_logger", BindingFlags.Instance | BindingFlags.NonPublic)!
                .SetValue(queryExecutor, serilogLogger);
            // Act and Assert
            var ex = await queryExecutor.QueryAsync(query);
            Assert.Equal(default, ex);
            Assert.NotNull(evt);
            Assert.Equal(LogEventLevel.Warning, evt.Level);
            Assert.Equal("Skipping query {QueryName} because feature toggle disabled", evt.MessageTemplate.Text);
        }
        [Fact]
        public async Task QueryAsync_Throws_NotAuthorisedException_When_Query_By_Unauthorised_User()
        {
            var userProvider = new Mock<IUserProvider>();
            userProvider.Setup(u => u.User).Returns(new FakeUser());
            var featureToggleService = new Mock<IFeatureToggleService>();
            featureToggleService.Setup(f => f.IsOnForType(It.IsAny<Type>())).Returns(true);
            var serviceProvider = new Mock<IServiceProvider>();
            var executor = new Mock<IQueryExecutor<TestQuery, string>>();
            executor.Setup(e => e.AuthoriseAsync(It.IsAny<TestQuery>(), It.IsAny<IUser>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(false);
            executor.Setup(e => e.ExecuteAsync(It.IsAny<TestQuery>(), It.IsAny<IUser>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync("Result");
            serviceProvider.Setup(s => s.GetService(typeof(IQueryExecutor<TestQuery, string>))).Returns(executor.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IUserProvider))).Returns(userProvider.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IFeatureToggleService))).Returns(featureToggleService.Object);

            var query = new TestQuery();
            var queryExecutor = new QueryExecutor(serviceProvider.Object);
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            // Replace the logger instance with the mock logger
            typeof(QueryExecutor)
                .GetField("_logger", BindingFlags.Instance | BindingFlags.NonPublic)!
                .SetValue(queryExecutor, serilogLogger);
            // Act and Assert
            var ex = await Assert.ThrowsAsync<UnauthorisedException<TestQuery>>(
                () => queryExecutor.QueryAsync(query));
            Assert.Equal("Failed to authorise user (NULL) when handling Application.UnitTest.Queries.Executor.TestQuery.", ex.Message);
            Assert.NotNull(evt);
            Assert.Equal(LogEventLevel.Warning, evt.Level);
            Assert.Equal("Execute query TestQuery: aborted", evt.MessageTemplate.Text);
        }
        [Fact]
        public async Task QueryAsync_Throws_Exception_When_Something_Wrong_With_Executor()
        {
            var userProvider = new Mock<IUserProvider>();
            userProvider.Setup(u => u.User).Returns(new FakeUser());
            var featureToggleService = new Mock<IFeatureToggleService>();
            featureToggleService.Setup(f => f.IsOnForType(It.IsAny<Type>())).Returns(true);
            var serviceProvider = new Mock<IServiceProvider>();
            var executor = new Mock<IQueryExecutor<TestQuery, string>>();
            executor.Setup(e => e.AuthoriseAsync(It.IsAny<TestQuery>(), It.IsAny<IUser>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);
            executor.Setup(e => e.ExecuteAsync(It.IsAny<TestQuery>(), It.IsAny<IUser>(), It.IsAny<CancellationToken>()))
                .ThrowsAsync(new Exception("FakeException"));
            serviceProvider.Setup(s => s.GetService(typeof(IQueryExecutor<TestQuery, string>))).Returns(executor.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IUserProvider))).Returns(userProvider.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IFeatureToggleService))).Returns(featureToggleService.Object);
            var query = new TestQuery();
            var queryExecutor = new QueryExecutor(serviceProvider.Object);
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            // Replace the logger instance with the mock logger
            typeof(QueryExecutor)
                .GetField("_logger", BindingFlags.Instance | BindingFlags.NonPublic)!
                .SetValue(queryExecutor, serilogLogger);
            // Act and Assert
            var ex = await Assert.ThrowsAsync<Exception>(
                () => queryExecutor.QueryAsync(query));
            Assert.Equal("FakeException", ex.Message);
            Assert.NotNull(evt);
            Assert.Equal(LogEventLevel.Warning, evt.Level);
            Assert.NotNull(evt.Exception);
            Assert.Equal("FakeException", evt.Exception.Message);
            Assert.Equal("Execute query {QueryName} {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
        }
        [Fact]
        public async Task QueryAsync_Returns_Result_When_Query_By_Authorised_User()
        {
            var userProvider = new Mock<IUserProvider>();
            userProvider.Setup(u => u.User).Returns(new FakeUser());
            var featureToggleService = new Mock<IFeatureToggleService>();
            featureToggleService.Setup(f => f.IsOnForType(It.IsAny<Type>())).Returns(true);
            var serviceProvider = new Mock<IServiceProvider>();
            var executor = new Mock<IQueryExecutor<TestQuery, string>>();
            executor.Setup(e => e.AuthoriseAsync(It.IsAny<TestQuery>(), It.IsAny<IUser>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);
            executor.Setup(e => e.ExecuteAsync(It.IsAny<TestQuery>(), It.IsAny<IUser>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync("FakeResult");
            serviceProvider.Setup(s => s.GetService(typeof(IQueryExecutor<TestQuery, string>))).Returns(executor.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IUserProvider))).Returns(userProvider.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IFeatureToggleService))).Returns(featureToggleService.Object);
            var query = new TestQuery();
            var queryExecutor = new QueryExecutor(serviceProvider.Object);
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                 .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                 .CreateLogger();
            // Replace the logger instance with the mock logger
            typeof(QueryExecutor)
                .GetField("_logger", BindingFlags.Instance | BindingFlags.NonPublic)!
                .SetValue(queryExecutor, serilogLogger);
            // Act and Assert
            var result = await queryExecutor.QueryAsync(query);
            Assert.Equal("FakeResult", result);
            Assert.NotNull(evt);
            Assert.Equal(LogEventLevel.Information, evt.Level);
            Assert.Equal("Execute query {QueryName} {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
        }
    }
    public class TestQuery : IQuery<TestQuery, string>
    {
    }
}
