﻿using Application.Services;
using Application.UnitTest.TestHelpers;
using Domain;
using Infrastructure.Persistence;
using Moq;
using Serilog;
using Serilog.Events;
using Application.UnitTest.TestHelpers.Serilog;
using Domain.Entities.FeatureFlags;
using Application.Queries.Employees;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;

namespace Application.UnitTest.Queries.Employees
{
    public class GetEmployeeByIdTests
    {
        private readonly IKensaDbContext _kensaDbContext;
        private readonly GetEmployeeById _query;
        private readonly IDistributedCache _distributedCache;
        private readonly IUser _fakeUser;
        private readonly Mock<IAuthorizationService> _mockAuthorizationService;

        public GetEmployeeByIdTests()
        {
            var mockUserProvider = new Mock<IUserProvider>();
            _kensaDbContext = new InMemoryKensaDbContext(mockUserProvider.Object);
            _query = new GetEmployeeById("testEmployeeId");
            _fakeUser = new FakeUser();

            //init a MemoryDistributedCache here
            var cacheOptions = Options.Create(new MemoryDistributedCacheOptions());
            _distributedCache = new MemoryDistributedCache(cacheOptions);
            _mockAuthorizationService = new Mock<IAuthorizationService>();
            _mockAuthorizationService.Setup(
                    x => x.IsAuthorizedForFeature(
                        It.IsAny<IUser>(), It.IsAny<Feature>()))
                .ReturnsAsync(() => true);
        }

        [Fact]
        public async Task AuthoriseAsync_AuthorizedFail_ReturnsFailed()
        {
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            var mockAuthorizationServiceFalse = new Mock<IAuthorizationService>();
            mockAuthorizationServiceFalse.Setup(
                    x => x.IsAuthorizedForFeature(
                        It.IsAny<IUser>(), It.IsAny<Feature>()))
                .ReturnsAsync(() => false);

            var executor = new GetEmployeeByIdExecutor(serilogLogger, 
                _kensaDbContext,
                _distributedCache,
                mockAuthorizationServiceFalse.Object);
            var result = await executor.AuthoriseAsync(_query, _fakeUser);
            Assert.False(result);
        }

        [Fact]
        public async Task AuthoriseAsync_AuthorizedSuccess_ReturnsTrue()
        {
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            var executor = new GetEmployeeByIdExecutor(serilogLogger,
                _kensaDbContext, 
                _distributedCache,
                _mockAuthorizationService.Object);
            var result = await executor.AuthoriseAsync(_query, _fakeUser);
            Assert.True(result);
        }
    }
}
