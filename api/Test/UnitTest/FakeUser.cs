﻿using Domain;

namespace Application.UnitTest
{
    internal class FakeUser : IUser
    {
        public string Email => null!;
        public string OrganisationId => null!;
        public string Id => null!;
        public bool IsImpersonating => false;
        public string? ImpersonatorEmail => null!;

        public string? IpAddress {get;set;}

        public int? Timezone => null!;

        public string? SessionId => null!;
        public void SetIpAddress(string? ipaddress)
        {

        }
    }
}
