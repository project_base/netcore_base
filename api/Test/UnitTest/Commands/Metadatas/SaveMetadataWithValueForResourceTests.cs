﻿using Application.Commands.Metadata;
using Application.Services;
using Application.UnitTest.TestHelpers.Serilog;
using Domain;
using Domain.Entities.FeatureFlags;
using Domain.Entities.Organisations;
using Infrastructure.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using Serilog;

namespace Application.UnitTest.Commands.Metadatas
{
	public class SaveMetadataWithValueForResourceTests
	{
		private readonly Mock<IKensaDbContext> _mockDbContext;
		private readonly Mock<IMediator> _mockMediator;
		private readonly Mock<IAuthorizationService> _mockAuthorizationService;
		private Mock<IAclService> _aclService;
		public SaveMetadataWithValueForResourceTests()
		{
			var groups = new Mock<DbSet<Group>>();
			groups.Setup(x => x.Add(It.IsAny<Group>())).Verifiable();
			_mockDbContext = new Mock<IKensaDbContext>();
			_mockDbContext
				.Setup(x => x.Groups)
				.Returns(groups.Object);
			_mockMediator = new Mock<IMediator>();
			_mockAuthorizationService = new Mock<IAuthorizationService>();
			_mockAuthorizationService.Setup(
					x => x.IsAuthorizedForFeature(
						It.IsAny<IUser>(), It.IsAny<Feature>()))
				.ReturnsAsync(() => true);

		}
		[Fact]
		public async Task Command_Invalid_If_ResourceId_Null_Empty()
		{
			var command = new SaveMetadataWithValueForResource("", ResourceType.Employee,
				[new SaveMetadataWithValueRequest(SaveMetadataAction.Add, "fakeMetadataId", "")]);
			Assert.False(command.IsValid());
			command = new SaveMetadataWithValueForResource(null!, ResourceType.Employee, [
				new SaveMetadataWithValueRequest(SaveMetadataAction.Add, "fakeMetadataId", "")
			]);
			Assert.False(command.IsValid());
		}
		[Fact]
		public async Task Command_Invalid_If_MetadataValues_Empty()
		{
			var command = new SaveMetadataWithValueForResource("fakeResourceId",
				ResourceType.Employee, []);
			Assert.False(command.IsValid());
		}
		[Fact]
		public async Task Command_Invalid_If_DuplicateMetadataValues()
		{
			var command = new SaveMetadataWithValueForResource("fakeResourceId",
				ResourceType.Employee, [
					new SaveMetadataWithValueRequest(SaveMetadataAction.Add, "fakeMetadataId1", "nn"),
					new SaveMetadataWithValueRequest(SaveMetadataAction.Update, "fakeMetadataId1", "aa")]);
			Assert.False(command.IsValid());
		}

		[Fact]
		public async Task AuthoriseAsync_Returns_False_If_Feature_IsNot_Allow()
		{
			var command = new SaveMetadataWithValueForResource("fakeResourceId",
				ResourceType.Employee, [
					new SaveMetadataWithValueRequest(SaveMetadataAction.Update, "fakeMetadataId1", "aa")]);
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => _ = e))
				.CreateLogger();
			_aclService = new Mock<IAclService>();
			_mockAuthorizationService.Setup(
					x => x.IsAuthorizedForFeature(
						It.IsAny<IUser>(), It.IsAny<Feature>()))
				.ReturnsAsync(() => false);
			var handler = new SaveMetadataWithValueForResourceHandler(_mockDbContext.Object,
				serilogLogger,
				_mockAuthorizationService.Object,
				_aclService.Object,
				_mockMediator.Object);
			var authoriseResult = await handler.AuthoriseAsync(command, new FakeUser());
			Assert.False(authoriseResult);
			command = new SaveMetadataWithValueForResource("fakeResourceId",
				ResourceType.Site, [
					new SaveMetadataWithValueRequest(SaveMetadataAction.Update, "fakeMetadataId1", "aa")]);
			authoriseResult = await handler.AuthoriseAsync(command, new FakeUser());
			Assert.False(authoriseResult);
			command = new SaveMetadataWithValueForResource("fakeResourceId",
				ResourceType.Job, [
					new SaveMetadataWithValueRequest(SaveMetadataAction.Update, "fakeMetadataId1", "aa")]);
			authoriseResult = await handler.AuthoriseAsync(command, new FakeUser());
			Assert.False(authoriseResult);
			command = new SaveMetadataWithValueForResource("fakeResourceId",
				(ResourceType)1000, [
					new SaveMetadataWithValueRequest(SaveMetadataAction.Update, "fakeMetadataId1", "aa")]);
			authoriseResult = await handler.AuthoriseAsync(command, new FakeUser());
			Assert.False(authoriseResult);
		}

		[Fact]
		public async Task AuthoriseAsync_Returns_False_If_Acl_Not_Pass()
		{
			var command = new SaveMetadataWithValueForResource("fakeResourceId",
				ResourceType.Employee, [
					new SaveMetadataWithValueRequest(SaveMetadataAction.Update, "fakeMetadataId1", "aa")]);
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => _ = e))
				.CreateLogger();
			_aclService = new Mock<IAclService>();
			var listAclOfSubjectPerResourceProperty = new List<AclOfSubjectPerResourceProperty>()
			{
				new AclOfSubjectPerResourceProperty("fakeUserId",
					ResourceType.Employee,
					"fakeMetadataId1",
					AclAccess.Deny, []
				)
			};
			_aclService.Setup(x => x.GetAclOfSubjectAgainstResourceProperties(
					It.IsAny<string>(), It.IsAny<string>(), It.IsAny<SubjectType>(),
					It.IsAny<ResourceType>(), It.IsAny<string[]>(), It.IsAny<CancellationToken>()))
				.ReturnsAsync(listAclOfSubjectPerResourceProperty);
			_mockAuthorizationService.Setup(
					x => x.IsAuthorizedForFeature(
						It.IsAny<IUser>(), It.IsAny<Feature>()))
				.ReturnsAsync(() => true);
			var handler = new SaveMetadataWithValueForResourceHandler(_mockDbContext.Object,
				serilogLogger,
				_mockAuthorizationService.Object,
				_aclService.Object,
				_mockMediator.Object);
			var authoriseResult = await handler.AuthoriseAsync(command, new BaseUser("fakeOrganisationId", "fakeEmail", "fakeUserId"));
			Assert.False(authoriseResult);
		}

		[Fact]
		public async Task AuthoriseAsync_Returns_True_If_Acl_Pass()
		{
			var command = new SaveMetadataWithValueForResource("fakeResourceId",
				ResourceType.Employee, [
					new SaveMetadataWithValueRequest(SaveMetadataAction.Update, "fakeMetadataId1", "aa")]);
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => _ = e))
				.CreateLogger();
			_aclService = new Mock<IAclService>();
			var listAclOfSubjectPerResourceProperty = new List<AclOfSubjectPerResourceProperty>()
			{
				new AclOfSubjectPerResourceProperty("fakeUserId",
					ResourceType.Employee,
					"fakeMetadataId1",
					AclAccess.Allow, []
				)
			};
			_aclService.Setup(x => x.GetAclOfSubjectAgainstResourceProperties(
					It.IsAny<string>(), It.IsAny<string>(), It.IsAny<SubjectType>(),
					It.IsAny<ResourceType>(), It.IsAny<string[]>(), It.IsAny<CancellationToken>()))
				.ReturnsAsync(listAclOfSubjectPerResourceProperty);
			_mockAuthorizationService.Setup(
					x => x.IsAuthorizedForFeature(
						It.IsAny<IUser>(), It.IsAny<Feature>()))
				.ReturnsAsync(() => true);
			var handler = new SaveMetadataWithValueForResourceHandler(_mockDbContext.Object,
				serilogLogger,
				_mockAuthorizationService.Object,
				_aclService.Object,
				_mockMediator.Object);
			var authoriseResult = await handler.AuthoriseAsync(command, new BaseUser("fakeOrganisationId", "fakeEmail", "fakeUserId"));
			Assert.True(authoriseResult);
		}
		[Fact]
		public async Task AuthoriseAsync_Returns_True_If_Acl_Not_Found()
		{
			var command = new SaveMetadataWithValueForResource("fakeResourceId",
				ResourceType.Employee, [
					new SaveMetadataWithValueRequest(SaveMetadataAction.Update, "fakeMetadataId1", "aa")]);
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => _ = e))
				.CreateLogger();
			_aclService = new Mock<IAclService>();
			var listAclOfSubjectPerResourceProperty = new List<AclOfSubjectPerResourceProperty>()
			{
				new AclOfSubjectPerResourceProperty("fakeUserId",
					ResourceType.Employee,
					"fakeMetadataId2",
					AclAccess.Deny, []
				)
			};
			_aclService.Setup(x => x.GetAclOfSubjectAgainstResourceProperties(
					It.IsAny<string>(), It.IsAny<string>(), It.IsAny<SubjectType>(),
					It.IsAny<ResourceType>(), It.IsAny<string[]>(), It.IsAny<CancellationToken>()))
				.ReturnsAsync(listAclOfSubjectPerResourceProperty);
			_mockAuthorizationService.Setup(
					x => x.IsAuthorizedForFeature(
						It.IsAny<IUser>(), It.IsAny<Feature>()))
				.ReturnsAsync(() => true);
			var handler = new SaveMetadataWithValueForResourceHandler(_mockDbContext.Object,
				serilogLogger,
				_mockAuthorizationService.Object,
				_aclService.Object,
				_mockMediator.Object);
			var authoriseResult = await handler.AuthoriseAsync(command, new BaseUser("fakeOrganisationId", "fakeEmail", "fakeUserId"));
			Assert.True(authoriseResult);
		}

	}
	
}
