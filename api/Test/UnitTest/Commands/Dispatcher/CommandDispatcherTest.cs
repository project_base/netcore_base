﻿using Application.Exceptions;
using Application.Services;
using Application.UnitTest.TestHelpers.Serilog;
using Moq;
using Serilog.Events;
using Serilog;
using System.Reflection;
using Application.Commands;
using Domain;

namespace Application.UnitTest.Commands.Dispatcher
{
    public class CommandDispatcherTest
    {
        [Fact]
        public async Task DispatchAsync_Returns_If_The_Feature_Is_Not_Enable()
        {
            var userProvider = new Mock<IUserProvider>();
            userProvider.Setup(u => u.User).Returns(new FakeUser());
            var featureToggleService = new Mock<IFeatureToggleService>();
            featureToggleService.Setup(f => f.IsOnForType(It.IsAny<Type>())).Returns(false);
            var serviceProvider = new Mock<IServiceProvider>();
            var dispatcher = new Mock<ICommandHandler<TestCommand>>();
            dispatcher.Setup(e => e.AuthoriseAsync(It.IsAny<TestCommand>(), It.IsAny<IUser>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);
            serviceProvider.Setup(s => s.GetService(typeof(ICommandHandler<TestCommand>))).Returns(dispatcher.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IUserProvider))).Returns(userProvider.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IFeatureToggleService))).Returns(featureToggleService.Object);
            var command = new TestCommand();
            var commandDispatcher = new CommandDispatcher(serviceProvider.Object);
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            // Replace the logger instance with the mock logger
            typeof(CommandDispatcher)
                .GetField("_logger", BindingFlags.Instance | BindingFlags.NonPublic)!
                .SetValue(commandDispatcher, serilogLogger);
			// Act and Assert
			var ex = await Assert.ThrowsAsync<FeatureNotEnabledException<TestCommand>>(
				() => commandDispatcher.DispatchAsync(command));
			Assert.Equal("Feature Application.UnitTest.Commands.Dispatcher.TestCommand is not enabled.", ex.Message);
			Assert.NotNull(evt);
            Assert.Equal(LogEventLevel.Warning, evt.Level);
            Assert.Equal("Skipping command {CommandName} because feature toggle disabled", evt.MessageTemplate.Text);
        }
        [Fact]
        public async Task DispatchAsync_Throws_NotAuthorisedException_When_Query_By_Unauthorised_User()
        {
            var userProvider = new Mock<IUserProvider>();
            userProvider.Setup(u => u.User).Returns(new FakeUser());
            var featureToggleService = new Mock<IFeatureToggleService>();
            featureToggleService.Setup(f => f.IsOnForType(It.IsAny<Type>())).Returns(true);
            var serviceProvider = new Mock<IServiceProvider>();
            var dispatcher = new Mock<ICommandHandler<TestCommand>>();
            dispatcher.Setup(e => e.AuthoriseAsync(It.IsAny<TestCommand>(), It.IsAny<IUser>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(false);
            serviceProvider.Setup(s => s.GetService(typeof(ICommandHandler<TestCommand>))).Returns(dispatcher.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IUserProvider))).Returns(userProvider.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IFeatureToggleService))).Returns(featureToggleService.Object);
            var command = new TestCommand();
            var commandDispatcher = new CommandDispatcher(serviceProvider.Object);
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            // Replace the logger instance with the mock logger
            typeof(CommandDispatcher)
                .GetField("_logger", BindingFlags.Instance | BindingFlags.NonPublic)!
                .SetValue(commandDispatcher, serilogLogger);
            // Act and Assert
            var ex = await Assert.ThrowsAsync<UnauthorisedException<TestCommand>>(
                () => commandDispatcher.DispatchAsync(command));
            Assert.Equal("Failed to authorise user (NULL) when handling Application.UnitTest.Commands.Dispatcher.TestCommand.", ex.Message);
            Assert.NotNull(evt);
            Assert.Equal(LogEventLevel.Warning, evt.Level);
            Assert.Equal("Calling command handler for TestCommand: aborted", evt.MessageTemplate.Text);
        }
        [Fact]
        public async Task DispatchAsync_Throws_NotAuthorisedException_When_Command_Is_Invalid()
        {
            var userProvider = new Mock<IUserProvider>();
            userProvider.Setup(u => u.User).Returns(new FakeUser());
            var featureToggleService = new Mock<IFeatureToggleService>();
            featureToggleService.Setup(f => f.IsOnForType(It.IsAny<Type>())).Returns(true);
            var serviceProvider = new Mock<IServiceProvider>();
            var dispatcher = new Mock<ICommandHandler<TestCommand>>();
            dispatcher.Setup(e => e.AuthoriseAsync(It.IsAny<TestCommand>(), It.IsAny<IUser>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(false);
            serviceProvider.Setup(s => s.GetService(typeof(ICommandHandler<TestCommand>))).Returns(dispatcher.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IUserProvider))).Returns(userProvider.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IFeatureToggleService))).Returns(featureToggleService.Object);
            var command = new TestCommand();
            command.SetInvalid();
            var commandDispatcher = new CommandDispatcher(serviceProvider.Object);
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            // Replace the logger instance with the mock logger
            typeof(CommandDispatcher)
                .GetField("_logger", BindingFlags.Instance | BindingFlags.NonPublic)!
                .SetValue(commandDispatcher, serilogLogger);
            // Act and Assert
            var ex = await Assert.ThrowsAsync<InvalidException<TestCommand>>(
                () => commandDispatcher.DispatchAsync(command));
            Assert.Equal("Command Application.UnitTest.Commands.Dispatcher.TestCommand is invalid to handle. Detail: ", ex.Message);
            Assert.NotNull(evt);
            Assert.Equal(LogEventLevel.Warning, evt.Level);
            Assert.Equal("Calling command handler for TestCommand: aborted", evt.MessageTemplate.Text);
        }
        [Fact]
        public async Task DispatchAsync_Throws_Exception_When_Something_Wrong_With_Executor()
        {
            var userProvider = new Mock<IUserProvider>();
            userProvider.Setup(u => u.User).Returns(new FakeUser());
            var featureToggleService = new Mock<IFeatureToggleService>();
            featureToggleService.Setup(f => f.IsOnForType(It.IsAny<Type>())).Returns(true);
            var serviceProvider = new Mock<IServiceProvider>();
            var dispatcher = new Mock<ICommandHandler<TestCommand>>();
            dispatcher.Setup(e => e.AuthoriseAsync(It.IsAny<TestCommand>(), It.IsAny<IUser>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);
            dispatcher.Setup(e => e.HandleAsync(It.IsAny<TestCommand>(), It.IsAny<IUser>(), It.IsAny<CancellationToken>()))
                .ThrowsAsync(new Exception("FakeException")); 
            serviceProvider.Setup(s => s.GetService(typeof(ICommandHandler<TestCommand>))).Returns(dispatcher.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IUserProvider))).Returns(userProvider.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IFeatureToggleService))).Returns(featureToggleService.Object);
            var command = new TestCommand();
            var commandDispatcher = new CommandDispatcher(serviceProvider.Object);
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            // Replace the logger instance with the mock logger
            typeof(CommandDispatcher)
                .GetField("_logger", BindingFlags.Instance | BindingFlags.NonPublic)!
                .SetValue(commandDispatcher, serilogLogger);
            // Act and Assert
            var ex = await Assert.ThrowsAsync<Exception>(
                () => commandDispatcher.DispatchAsync(command));
            Assert.Equal("FakeException", ex.Message); 
            Assert.NotNull(evt);
            Assert.Equal(LogEventLevel.Warning, evt.Level);
            Assert.NotNull(evt.Exception);
            Assert.Equal("FakeException", evt.Exception.Message);
            Assert.Equal("Calling command handler for {CommandName} {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
        }
        [Fact]
        public async Task DispatchAsync_Process_When_Dispatch_By_Authorised_User()
        {
            var userProvider = new Mock<IUserProvider>();
            userProvider.Setup(u => u.User).Returns(new FakeUser());
            var featureToggleService = new Mock<IFeatureToggleService>();
            featureToggleService.Setup(f => f.IsOnForType(It.IsAny<Type>())).Returns(true);
            var serviceProvider = new Mock<IServiceProvider>();
            var dispatcher = new Mock<ICommandHandler<TestCommand>>();
            dispatcher.Setup(e => e.AuthoriseAsync(It.IsAny<TestCommand>(), It.IsAny<IUser>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);
            dispatcher.Setup(e => e.HandleAsync(It.IsAny<TestCommand>(), It.IsAny<IUser>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(new CommandHandlerResult("")));
            serviceProvider.Setup(s => s.GetService(typeof(ICommandHandler<TestCommand>))).Returns(dispatcher.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IUserProvider))).Returns(userProvider.Object);
            serviceProvider.Setup(s => s.GetService(typeof(IFeatureToggleService))).Returns(featureToggleService.Object);
            var command = new TestCommand();
            var commandDispatcher = new CommandDispatcher(serviceProvider.Object);
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            // Replace the logger instance with the mock logger
            typeof(CommandDispatcher)
                .GetField("_logger", BindingFlags.Instance | BindingFlags.NonPublic)!
                .SetValue(commandDispatcher, serilogLogger);
            // Act and Assert
            await commandDispatcher.DispatchAsync(command);
            Assert.NotNull(evt);
            Assert.Equal(LogEventLevel.Information, evt.Level);
            Assert.Equal("Calling command handler for {CommandName} {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class TestCommand : ICommand
    {
        private bool _isValid = true;

        public bool IsValid()
        {
            return _isValid;
        }
        public void SetInvalid()
        {
            _isValid = false;
        }
    }

}
