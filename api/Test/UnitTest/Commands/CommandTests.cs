﻿using System.Reflection;
using Application.Commands;

namespace Application.UnitTest.Commands
{
    public class CommandTests
    {
        [Fact]
        public void AllCommandsShouldHaveParameterlessConstructor()
        {
            var commandTypes = Assembly.GetAssembly(typeof(ICommand)).GetTypes()
                .Where(t => typeof(ICommand).IsAssignableFrom(t) && !t.IsInterface && !t.IsAbstract);

            foreach (var commandType in commandTypes)
            {
                var constructors = commandType.GetConstructors();
                Assert.True(constructors.Any(c => c.GetParameters().Length == 0),
                    $"{commandType.Name} does not have a parameterless constructor.");
            }
        }
    }
}
