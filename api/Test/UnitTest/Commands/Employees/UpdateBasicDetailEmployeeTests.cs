﻿using Application.Commands.Employees;
using Application.Services;
using Application.UnitTest.TestHelpers.Serilog;
using Domain;
using Domain.Entities.FeatureFlags;
using Infrastructure.Persistence;
using MediatR;
using Moq;
using Serilog;

namespace Application.UnitTest.Commands.Employees
{
    public class UpdateBasicDetailEmployeeTests
	{
        private readonly Mock<IKensaDbContext> _mockDbContext;
        private readonly Mock<IMediator> _mockMediator;

        public UpdateBasicDetailEmployeeTests()
        {
	        _mockDbContext = new Mock<IKensaDbContext>();
            _mockMediator = new Mock<IMediator>();
        }
        [Fact]
        public void Command_Invalid_If_Missing_Id_Or_FirstName_Or_LastName()
        {
            var command = new UpdateBasicDetailEmployee();
            Assert.False(command.IsValid());
            command = new UpdateBasicDetailEmployee("","fakeFirstName","fakeLastName");
            Assert.False(command.IsValid());
            command = new UpdateBasicDetailEmployee("aaa@aaa", "", "fakeLastName");
            Assert.False(command.IsValid());
            command = new UpdateBasicDetailEmployee("aaa@aaa", "fakeFirstName", "");
            Assert.False(command.IsValid());
		}
        [Fact]
        public async Task AuthoriseAsync_Returns_False_If_Authorized_False()
        {
			var command = new UpdateBasicDetailEmployee("aaa@aaa", "fakeFirstName", "fakeLastName");
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => _ = e))
                .CreateLogger();
            var mockAuthorizationServiceFalse = new Mock<IAuthorizationService>();
            mockAuthorizationServiceFalse.Setup(
                    x => x.IsAuthorizedForFeature(
                        It.IsAny<IUser>(), It.IsAny<Feature>()))
                .ReturnsAsync(() => false);
            var handler = new UpdateBasicDetailEmployeeHandler(
                _mockDbContext.Object,
                serilogLogger,
                mockAuthorizationServiceFalse.Object,
				_mockMediator.Object);
            var authoriseResult = await handler.AuthoriseAsync(command, null!);
            Assert.False(authoriseResult);
        }
    }
}
