using Domain.Entities.Notifications;

namespace Domain.UnitTest.Entities.Notifications
{
    public class NotificationMessageTests
    {
        [Fact]
        public void EmployeeRequestOtpForPasswordResetNotificationMessage_HasCorrectData()
        {
	        // Arrange
	        string organisationName = "Sample Organization";
	        string fullname = "John Doe";
	        string otp = "123456";

	        // Act
	        var notificationMessage = new EmployeeRequestOtpForPasswordResetNotificationMessage(organisationName, fullname, otp);

	        // Assert
	        Assert.Equal(NotificationType.EmployeeRequestedOtpForPasswordReset, notificationMessage.Type);
	        Assert.Equal(3, notificationMessage.Data.Count);
	        Assert.Equal(otp, notificationMessage.Data["Otp"]);
	        Assert.Equal(fullname, notificationMessage.Data["FullName"]);
	        Assert.Equal(organisationName, notificationMessage.Data["OrganisationName"]);

		}

        [Fact]
        public void LockedOutNotificationMessage_HasCorrectData()
        {
	        // Arrange
	        string organisationName = "Sample Organization";
	        string fullname = "John Doe";

	        // Act
	        var notificationMessage = new LockedOutNotificationMessage(organisationName, fullname);

	        // Assert
	        Assert.Equal(NotificationType.EmployeeLockedOut, notificationMessage.Type);
	        Assert.Equal(2, notificationMessage.Data.Count);
	        Assert.Equal(fullname, notificationMessage.Data["FullName"]);
	        Assert.Equal(organisationName, notificationMessage.Data["OrganisationName"]);

        }
	}
}