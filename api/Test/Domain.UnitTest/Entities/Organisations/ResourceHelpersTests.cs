﻿using Domain.Entities;
using Domain.Entities.Organisations;
using System.Linq.Expressions;

namespace Domain.UnitTest.Entities.Organisations
{
	public class ResourceHelpersTests
	{
		[Theory]
		[InlineData(ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_ID, "Id")]
		[InlineData(ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_EMAIL, "Email")]
		[InlineData(ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_AVATAR, "Avatar")]
		[InlineData(ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_FIRST_NAME, "FirstName")]
		[InlineData(ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_LAST_NAME, "LastName")]
		[InlineData("Unknown", "")]

		public void GetSqlColumnName_Returns_Correct_Name(string columnId, string expectedColumnName)
		{
			Assert.Equal(expectedColumnName, columnId.GetSqlColumnName());
		}
		[Theory]
		[InlineData(ResourceType.Employee, "Id", ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_ID)]
		[InlineData(ResourceType.Employee, "Email", ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_EMAIL)]
		[InlineData(ResourceType.Employee, "Avatar", ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_AVATAR)]
		[InlineData(ResourceType.Employee, "FirstName", ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_FIRST_NAME)]
		[InlineData(ResourceType.Employee, "LastName", ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_LAST_NAME)]
		[InlineData(ResourceType.Employee, "Unknown", "")]
		[InlineData((ResourceType)999, "Unknown", "")]

		public void GetColumnIdFromSqlColumnName_Returns_Correct_Id(ResourceType resourceType, string sqlColumnName, string expectedColumnId)
		{
			Assert.Equal(expectedColumnId, sqlColumnName.GetColumnIdFromSqlColumnName(resourceType));
		}
		[Theory]
		[InlineData(ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_ID, true)]
		[InlineData(ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_EMAIL, true)]
		[InlineData("Unknown", false)]
		public void IsDefaultColumn_Returns_Correct_Value(string columnId, bool expectedIsDefault)
		{
			Assert.Equal(expectedIsDefault, columnId.IsDefaultColumn());
		}
		[Theory]
		[InlineData(ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_ID, false)]
		[InlineData(ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_EMAIL, true)]
		[InlineData(ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_AVATAR, false)]
		[InlineData("Unknown", false)]
		public void CanFilter_Returns_Correct_Value(string columnId, bool expectedIsDefault)
		{
			Assert.Equal(expectedIsDefault, columnId.CanFilter());
		}

		[Theory]
		[MemberData(nameof(GetDefaultColumnsOfResourcesData))]
		public void GetDefaultColumnsOfResources_Returns_Correct_ListColumns(ResourceType resourceType, int expectedCount)
		{
			var result = resourceType.GetDefaultColumnsOfResources();
			Assert.Equal(expectedCount, result.Length);
		}
		[Fact]
		public void GetDefaultColumnsOfResources_Throws_Exception_If_Wrong_Parameter()
		{
			var unsupportedResourceType = (ResourceType)999; // Assuming 999 is not a valid ResourceType
			Assert.Throws<ArgumentOutOfRangeException>(() => unsupportedResourceType.GetDefaultColumnsOfResources());
		}
		public static TheoryData<ResourceType, int> GetDefaultColumnsOfResourcesData =>
			new()
			{
				{ ResourceType.Employee, 5 },
				{ ResourceType.Site, 3 },
				{ ResourceType.Job, 2 }
			};

		[Fact]
		public void GetFilterExpressionForColumn_Returns_PropertyExpression_For_Employee_Email()
		{
			// Arrange

			// Act
			var expression = ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_EMAIL.GetFilterExpressionForColumn<Employee>();

			// Assert
			Assert.NotNull(expression);
			Assert.Equal(typeof(Employee), expression.Parameters[0].Type);
			Assert.Equal(nameof(Employee.Email), ((MemberExpression)expression.Body).Member.Name);
		}

		[Theory]
		[MemberData(nameof(GetFilterExpressionForColumnsData_ForEmployee))]
		public void GetFilterExpressionForColumn_Returns_Correct_PropertyExpression(string columnId,
			//ResourceType resourceType,
			Type type,
			string columnName)
		{
			// Act
			var expression = columnId.GetFilterExpressionForColumn<Employee>();

			// Assert
			Assert.NotNull(expression);
			Assert.Equal(type, expression.Parameters[0].Type);
			Assert.Equal(columnName, ((MemberExpression)expression.Body).Member.Name);
		}
		/// <summary>
		/// its for Employee entity, we need to add more later
		/// </summary>
		public static TheoryData<string, Type, string> GetFilterExpressionForColumnsData_ForEmployee =>
			new()
			{
				{ ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_FIRST_NAME, typeof(Employee), nameof(Employee.FirstName) },
				{ ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_LAST_NAME, typeof(Employee), nameof(Employee.LastName) },
				{ ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_EMAIL, typeof(Employee), nameof(Employee.Email) },
			};
		[Fact]
		public void GetFilterExpressionForColumn_DefaultCase_ReturnsEmptyString()
		{
			// Arrange
			string columnId = "unknownColumnId";

			// Act
			var expression = columnId.GetFilterExpressionForColumn<Employee>();

			// Assert
			Assert.Equal(string.Empty, expression.Compile()(new Employee()));
		}

		[Theory]
		[MemberData(nameof(GetSelectorDelegateForColumn_ForEmployee))]
		public void GetSelectorDelegateForColumn_Returns_CorrectSelector_For_Employee(
			Employee employee,
			string columnId,
			object? expected)
		{
			var selector = columnId.GetSelectorDelegateForColumn<Employee>();
			// Assert
			Assert.NotNull(selector);
			var result = selector(employee);
			Assert.Equal(expected, result);
		}

		/// <summary>
		/// its for Employee entity, we need to add more later
		/// </summary>
		public static TheoryData<Employee, string, object> GetSelectorDelegateForColumn_ForEmployee
		{
			get
			{
				var fakeId = "fakeId";
				var fakeOrgId = "fakeOrgId";
				var fakeEmail = "fakeEmail";
				var fakeFirstName = "fakeFirstName";
				var fakeLastName = "fakeLastName";
				var employee = new Employee(id: fakeId,
					firstName: fakeFirstName,
					lastName: fakeLastName,
					email: fakeEmail,
					organisationId: fakeOrgId
				);
				return new()
				{
					{ employee,ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_EMAIL, fakeEmail },
					{ employee,ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_FIRST_NAME, fakeFirstName },
					{ employee,ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_LAST_NAME, fakeLastName },
					{ employee,ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_AVATAR, string.Empty },
					{ employee,"unknownId", null! },
				};
			}
		}

		[Fact]
		public void FilterColumnByExpressionAndContainMethod_FiltersQueryByStringProperty()
		{
			// Arrange
			var query = new[]
			{
				new TestEntity { Name = "Rose", Description = "Flower", Id=1},
				new TestEntity { Name = "Banana", Description = "fruit", Id=2 },
				new TestEntity { Name = "Orange", Description = "fruit", Id=3 }
			}.AsQueryable();

			var result = query.FilterColumnByExpressionAndContainMethod(e => e.Name, "an");
			Assert.Equal(2, result.Count());
			result = query.FilterColumnByExpressionAndContainMethod(e => e.Description, "er");
			Assert.NotEmpty(result);
			result = query.FilterColumnByExpressionAndContainMethod(e => e.Description, "ddda");
			Assert.Empty(result);
			result = query.FilterColumnByExpressionAndContainMethod(e => e.Id, "1");
			Assert.NotEmpty(result);
			result = query.FilterColumnByExpressionAndContainMethod(e => e.Id, "4");
			Assert.Empty(result);
		}
		private class TestEntity : IDbEntity
		{
			public string Name { get; set; }
			public string Description { get; set; }
			public int Id { get; set; }
			public void MarkAsCreated(string? createdBy) { }
			public void MarkAsModified(string? modifiedBy) { }
		}
	}
}
