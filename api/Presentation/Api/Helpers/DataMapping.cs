using Application.Controllers.Manager.Role.Presenter;
using Application.Controllers.PublicCtrl.Presenter;
using AutoMapper;
using Domain;
using Domain.Schemas;

namespace Application.Helpers
{
    public class DataMapping : Profile
    {
        public DataMapping()
        {
            // Default mapping when property names are same
            CreateMap<LoginPresenter, LoginModel>();
            CreateMap<RegisterPresenter, RegisterModel>();
            CreateMap<TokenPresenter, TokenModel>();
            CreateMap<CreateRolePresenter, RoleSchema>();


            // Mapping when property names are different
            //CreateMap<User, UserViewModel>()
            //    .ForMember(dest =>
            //    dest.FName,
            //    opt => opt.MapFrom(src => src.FirstName))
            //    .ForMember(dest =>
            //    dest.LName,
            //    opt => opt.MapFrom(src => src.LastName));
        }
    }
}
