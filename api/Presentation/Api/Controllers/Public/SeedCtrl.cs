﻿using Application.Routers;
using Core.Models;
using AutoMapper;
using Core;
using Infrastructure.Services;

namespace Application.Controllers.PublicCtrl
{
    public class SeedCtrl : BaseController
    {
        private readonly SeedDataService seedService;

        public SeedCtrl(HttpContext _httpContext, DataContext ctx, byte[] _secretKey, IMapper _mapper)
            : base(_httpContext, ctx, _secretKey, _mapper)
        {
            seedService = new SeedDataService(ctx);
        }

        public async Task<IResult> Execute()
        {
            var routers = new ZRouterManager(secretKey).Get();
            ResponseModel response = await seedService.Execute(routers);
            return Results.Ok(response);
        }
    }
}
