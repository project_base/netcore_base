﻿using Core;
using Domain.Schemas;
using Microsoft.EntityFrameworkCore;

namespace Application.Controllers
{
    public class PermCtrl
    {
        private readonly DataContext dataContext;
        public PermCtrl(DataContext dbContext)
        {
            dataContext = dbContext;
        }

        public async Task<IResult> GetAllAsync()
        {
            var response = await dataContext.Set<PermSchema>().ToListAsync();
            return Results.Ok(response);
        }
    }
}
