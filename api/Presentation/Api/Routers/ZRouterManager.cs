﻿using Core.Models;

namespace Application.Routers
{
    public class ZRouterManager
    {
        private readonly byte[] secretKey;
        public ZRouterManager(byte[] _secretKey)
        {
            secretKey = _secretKey;
        }
        public List<RouterModel> Get()
        {
            List<RouterModel> routers = new List<RouterModel>();

            var routersToAdd = new List<IEnumerable<RouterModel>>
            {
                new PublicRouter         ().Get(secretKey),
                new PermRouter           ().Get(),
            };

            foreach (var routerList in routersToAdd)
            {
                routers = routers.Union(routerList).ToList();
            }

            return routers;
        }

    }
}
