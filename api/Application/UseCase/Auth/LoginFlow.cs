﻿using Core.Models;
using Core;
using Microsoft.EntityFrameworkCore;
using Domain;
using Domain.Schemas; 
using Core.Domain.Business;
using Domain.Util;

namespace UseCase
{
    public class LoginFlow
    {
        private readonly DataContext dataContext;
        public LoginFlow(DataContext ctx)
        {
            dataContext = ctx;
        }
         
        public ResponseModel Execute(LoginModel model, byte[] secretKey)
        {
            UserSchema user = dataContext.Users.Where(u => u.Email.Equals(model.Email)).FirstOrDefault();
            if (user == null)
            {
                throw new Exception("User not found");
            } 
            bool isMatched = JwtUtil.Compare(model.Password, user.Password);
            if (!isMatched)
            {
               throw new Exception("Password not match");
            }
            UserRoleModel userPermModel = dataContext.Users
               .Where(u => u.Id == user.Id)
               .Include(u => u.UserRoles)
                   .ThenInclude(ur => ur.Role)
                       .ThenInclude(r => r.RolesPerms)
                           .ThenInclude(rp => rp.Perm)
               .Select(u => UserRule.GetUserRole(u))
               .FirstOrDefault();

            UserRoleModel userPerm = userPermModel;
            string accessToken = JwtUtil.GenerateAccessToken(userPerm, secretKey);
            string refreshToken = JwtUtil.GenerateRefreshToken(); 
            UserSchema? u = dataContext.Users.Find(user.Id);
            if (u != null)
            {
                u.HashRefreshToken = refreshToken;
                u.LastLogin = DateTime.UtcNow;
                dataContext.SaveChanges();
            }
            return new ResponseModel(GlobalVariable.SUCCESS, new { AccessToken = accessToken, RefreshToken = refreshToken, User = userPerm });
        }
    }
}
